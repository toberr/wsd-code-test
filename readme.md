# Wallstreetdocs demo

## install
  - execute "npm install"
  - copy .env.example to .env
  - run the app with "npm run dev"
  - open the app in the browser on localhost:3000

## npm commands
  - npm run dev - development mode
  - npm run prod - production mode (it will not work for testing (secure cookie))
  - npm run test - a few tests with: supertest - mocha - chai

## development workflow
  - technologies
    - grunt 
    - scss
    - uglifyjs
    - browsersync
    - nodemon
    - eslint

- The app use environment variables, they are coming from a .env file in testing,
in production mode it should set on the operation system for example with a systemd service.
- In development mode the app will watch for file changes, and it will reload the server/browser
- Nodemon detects the file changes in the server files, but there is some delay before the app restarted, that's why I'm updating the .server-restart file after the app ready to trigger a browser reload with browsersync
- eslint will catch syntax errors in the editor, (there is an es5 config for the frontend and es6-node config for the backend)  
if you are using vscode it will check the html files too for the embedded javascript in the ejs tags (https://www.npmjs.com/package/eslint-plugin-lodash-template)

## app details
  - technologies
    - "express" framework
    - "debug" for logging
    - "ejs" template engine
  - routes
    - "/" redirects to "/dashboard" without the "code" parameter
    - "/dashboard " start page
    - "/profile" profile page requires login
    - "/auth/wsd/login" login start route
    - "/auth/wsd/logout" logout route
    - "/" oauth callback url it will trigger the process if it has the "code" parameter


## tests
  - I wrote a few simple tests, you can start it with the "npm run test" command
  - I couldn't test the oauth login process, probably it can be done with Puppeteer / Selenium
  
## jquery module
  - analytics plugin there is an url and a callback paramter in the function object
  - it will send keypress, click events to the server "/api/analtics" with ajax
