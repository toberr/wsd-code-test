const debug = require('debug')('wsd:errorHandler');
const render = require('../utils/render');

function errorHandler(err, req, res, next) {
  debug(req.url);
  debug(err.stack);
  
  const isDev = process.env.NODE_ENV === 'development';
  let errorStatus = err.status || 500;
  let errorMessage = err.toString() || 'Internal server error';

  const templateData = {
    template: 'error',
    title: 'error',
    bodyClass: 'error',
    errorMessage,
    errorStatus
  };
  
  if (isDev) {
    templateData.stackTrace = err.stack;
  }

  res.status(isDev ? errorStatus : 500);

  if (req.headers.accept === 'application/json') {
    return res.send({ error: templateData.errorMessage || 'Internal server error' });
  }

  render(req, res, templateData);
}

module.exports = errorHandler;
