const render = require('../utils/render');

module.exports = function (req, res, next) {
  render(req, res, {
    template: 'error-404',
    title: '404',
    bodyClass: 'e-404',
  });
};