require('dotenv').config();

const express = require('express');
const path = require('path');
const fs = require('fs');
const ejs = require('ejs');
const passport = require('passport');
const debug = require('debug')('wsd:app');
const session = require('express-session');
const flash = require('connect-flash');

const router = require('./router/router');
const errorHandler = require('./error/errorHandler');
const error404 = require('./error/error404');

const PORT = process.env.PORT || 8000;
const HOST = process.env.HOST || 'localhost';
const isDev = (process.env.NODE_ENV == 'development');

const app = express();
const projectRoot = process.cwd();
process.title = 'wsd';

function webApp() {
  function initPassport() {
    require('./config/passport-setup');
  }
  
  function initSession() {
    let sess = {
      secret: process.env.SESSION_SECRET,
      cookie: {},
      resave: false,
      saveUninitialized: false,
    };
    
    if (!isDev) {
      app.set('trust proxy', 1);
      sess.cookie.secure = true;
    }
    
    app.use(session(sess));
  }
  
  function initFlash() {
    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());
  }
  
  function initViewEngine() {
    app.locals.ejs = {
      NODE_ENV: process.env.NODE_ENV
    };
    
    const ejsOptions = {
      root: path.join(process.cwd(), '/server/views'),
      //rmWhitespace: true
    };
    
    app.engine('html', (path, data, cb) => {
      ejs.renderFile(path, data, ejsOptions, cb);
    });
    
    app.set('view engine', 'html');
    app.set('views', ejsOptions.root);
    
    // template globals
    app.use((req, res, next) => {
      // debug('req.session', req.session);
      res.locals.login = req.isAuthenticated();
      res.locals.session = req.session;
      next();
    });
  }
  
  function initRouter() {
    app.use(router);
  }
  
  function initStaticFiles() {
    app.use('/build', express.static(path.join(`${projectRoot}/client/build`)));
    if (isDev) {
      app.use('/build', express.static(path.join(`${projectRoot}/client/src`)));
    }
  }
  
  function initErrorHandler() {
    // 404 not found page
    app.use(error404);
  
    //error handler
    app.use(errorHandler);
  }
  
  function initApp() {
    app.listen(PORT, HOST, () => {
      debug(`${process.title} listening on http://${HOST}:${PORT}`);
      if (isDev) {
        fs.writeFileSync(`${process.cwd()}/.server-restart`, '');
      }
    });
  }
  
  initPassport();
  initSession();
  initFlash();
  initViewEngine();
  initRouter();
  initStaticFiles();
  initErrorHandler();
  initApp();
  
  return app;
}

module.exports = webApp;



