/* process.env.NODE_ENV = 'test';
require('dotenv').config({path: '.env.test'}); */
const debug = require('debug')('wsd:test');

const webApp = require('../app');
const app = webApp();

require('./test')(app);
