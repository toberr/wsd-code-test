/* global describe it */
const request = require('supertest');
const chai = require('chai');
const assert = chai.assert;

module.exports = function(app) {
  describe('application', () => {
    // let cookie, lastName;
    
    it('"/" redirects to "/dashboard" without code parameter"', function (done) {
      request(app)
        .get('/')
        .expect('Location', /dashboard/)
        .expect(302, done);
    });
    
    it('"/profile" redirects to "/dashboard" without login"', function (done) {
      request(app)
        .get('/')
        .expect('Location', /dashboard/)
        .expect(302, done);
    });
    
    it('"/api/analytics" responds with json ok for json { type: \'keypress\', keycode: 10} data"', function (done) {
      request(app)
        .post('/api/analytics')
        .set('Accept', 'application/json')
        .send({ type: 'keypress', keycode: 10})
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, response) {
          if (err) { return done(err); }
          assert.equal(response.status, 200);
          assert.deepEqual(response.body, { status: 'ok' });
          done();
        });
    });
    
  });
};
