const passport = require('passport');
const debug = require('debug')('wsd:auth-controller');
const { getUserProfile } = require('../utils/utils');
const render = require('../utils/render');

function ensureLoggedIn(req, res, next) {
  debug('ensureLoggedIn', {
    originalUrl: req.originalUrl, isAuthenticated: req.isAuthenticated(), xhr: req.xhr, method: req.method
  });
  if (!req.isAuthenticated()) {
    if (req.xhr) {
      return res.sendStatus(401);
    } else {
      return res.redirect('/dashboard');
    }
  } else {
    next();
  }
}

// oauth with wallstreetdocks provider
exports.wsdLogin = passport.authenticate('wsd'/* , { scope: ['?'] } */);

exports.wsdLogout = (req, res) => {
  req.logout();
  if (req.session) {
    req.session.destroy((err) => {
      if (err) {
        debug('session.destroy err', err);
      }
      debug('Destroyed the user session on Auth0 endpoint');
      res.redirect('/dashboard');
    });
  } else {
    res.redirect('/dashboard');
  }
};

// oauth with wallstreetdocks redirect
exports.wsdRedirect = [
  (req, res, next) => {
    debug('req.query.code', req.query.code);
    if (!req.query.code) {
      res.redirect('/dashboard');
    } else {
      next();
    }
  },
  passport.authenticate('wsd', { 
    successRedirect: '/dashboard', 
    failureRedirect: '/dashboard' ,
    failureFlash: true,
  })
];

// user profile - wallstreetdocks
exports.wsdProfile = [
  ensureLoggedIn,
  (req, res) => {
    getUserProfile(req.session.oauth.accessToken)
      .then(profile => {
        // profile.body, profile.headers, profile.status
        render(req, res, {
          template: 'profile',
          title: 'profile',
          bodyClass: 'profile',
          profile: profile.body
        });
      })
      .catch(err => {
        debug('user profile error', err);
        res.send('user profile error', null);
      });
  }
];

exports.ensureLoggedIn = ensureLoggedIn;
