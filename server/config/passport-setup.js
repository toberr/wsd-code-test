const passport = require('passport');
const OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
const debug = require('debug')('wsd:passport');
const { getUserProfile } = require('../utils/utils');

passport.serializeUser((user, done) => {
  debug('user', user.id);
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  /* User.findById(id).then((user) => {
    done(null, user);
  }); */
  done(null, id);
});

passport.use('wsd', new OAuth2Strategy({
  authorizationURL: process.env.OAUTH_WSD_AUTHORIZATION_URL,
  tokenURL: process.env.OAUTH_WSD_TOKEN_URL,
  clientID: process.env.OAUTH_WSD_CLIENT_ID,
  clientSecret: process.env.OAUTH_WSD_CLIENT_SECRET,    
  callbackURL: process.env.OAUTH_WSD_CALLBACK_URL,
  passReqToCallback: true
},
function(req, accessToken, refreshToken, profile, done) {
  debug({accessToken, refreshToken, profile, done});
  //User.findOrCreate(..., function(err, user) {
  //  done(err, user);
  //});
  
  getUserProfile(accessToken)
    .then(res => {
      // res.body, res.headers, res.status
      debug('res.body', res.body);
      req.session.oauth = {
        accessToken,
        refreshToken
      };
      done(null, {id: res.body.id});
    })
    .catch(err => {
      debug('passport error', err.message);
      req.flash('error', err.message);
      done(null, false);
    });
}
));