const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');

router.get('/auth/wsd/login', authController.wsdLogin);
router.get('/auth/wsd/logout', authController.wsdLogout);
router.get('/profile', authController.wsdProfile);
router.get('/', authController.wsdRedirect);

module.exports = router;
