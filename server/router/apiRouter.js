const express = require('express');
const router = express.Router();
const debug = require('debug')('wsd:api');

router.use(express.json());

router.post('/analytics', (req, res) => {
  debug('req.body', req.body);
  res.status(200).json({status: 'ok'});
});

module.exports = router;
