const express = require('express');
const router = express.Router();
const render = require('../utils/render');

router.get('/dashboard', (req, res) => {
  render(req, res, {
    template: 'index',
    title: 'index title',
    bodyClass: 'dashboard',
    error: req.flash('error')
  });
});

module.exports = router;