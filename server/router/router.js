const express = require('express');
const router = express.Router();
const baseRouter = require('./baseRouter');
const authRouter = require('./authRouter');
const apiRouter = require('./apiRouter');

router.use('/api', apiRouter);
router.use('/', authRouter);
router.use('/', baseRouter);

module.exports = router;
