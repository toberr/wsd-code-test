const debug = require('debug')('wsd:ejs-render');
const _ = require('lodash');

module.exports = function render(req, res, config) {
  let templateData = {
    template: config.template,
    data: Object.assign({
      NODE_ENV: process.env.NODE_ENV,
      login: res.locals.login,
      url: req.originalUrl,
      locals: res.locals,
    }, config),
    q: (s, defaultValue = '') => {
      let result = _.get(templateData.data, s, defaultValue);
      return result === '' ? defaultValue : result;
    },
    _
  };

  // debug('templateData:', templateData);
  debug({url: req.originalUrl, template: config.template});
  res.render('components/layout.html', templateData);
};
