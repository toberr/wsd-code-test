const superagent = require('superagent');

function getUserProfile(accessToken) {
  return new Promise((resolve, reject) => {
    resolve(superagent.get('https://staging-auth.wallstreetdocs.com/oauth/userinfo')
      .set('Authorization', `Bearer ${accessToken}`)
      .set('Cache-Control', 'no-cache'));
  });
}

module.exports = {
  getUserProfile
};