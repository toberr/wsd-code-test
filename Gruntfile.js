require('dotenv').config();
const sass = require('node-sass');
var browserSync = require('browser-sync');

module.exports = function(grunt) {
  
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        sourceMap: {
          root: './',
          includeSources: true
        },
        compress: {
          unused: false
        },
        output: {
          quote_style: 3
        }
      },
      build: {
        src: 'client/src/js/main.js',
        dest: 'client/build/js/main.js',
      }
    },
    
    sass: {
      options: {
        implementation: sass,
        sourceMap: true,
        sourceMapContents: true,
      },
      dist: {
        files: {
          'client/build/css/main.css': 'client/src/scss/main.scss'
        }
      }
    },
    
    watch: {
      style: {
        files: ['client/src/scss/**/*.scss'],
        tasks: ['sass', 'bs-reload'],
        options: {
          spawn: false,
        },
      },
      other: {
        files: [
          'client/src/js/**/*.js',
          'client/src/img/**/*',
          'server/views/**/*.html',
          '.server-restart'
        ],
        tasks: ['bs-reload'],
        options: {
          spawn: false,
        },
      },
    },
  });
  
  // Init BrowserSync manually
  grunt.registerTask('bs-init', function () {
    var done = this.async();
    console.log('test', `${process.env.HOST}:${process.env.PORT}`);
    browserSync({
      proxy: `${process.env.HOST}:${process.env.PORT}`,
    }, function (err, bs) {
      done();
    });
  });

  // Inject CSS files to the browser
  grunt.registerTask('bs-reload', function () {
    console.log('reload');
    browserSync.reload(
      // [ 'client/build/**/*.{js,css}' ]
    );
  });
  
  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  //task(s).
  grunt.registerTask('default', ['sass', 'uglify']);
  grunt.registerTask('dev', ['sass', 'bs-init', 'watch']);
};