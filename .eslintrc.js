module.exports = {
  'env': {
    'node': true,
    'es6': true
  },
  'parserOptions': {
    'ecmaVersion': 2016,
  },
  'plugins': [
    'import',
  ],
  'globals': {
    '$': true,
    'jQuery': true,
  },
  'extends': [
    'plugin:lodash-template/base'
  ],
  'rules': {
    'indent': [
      'error',
      2,
      {'SwitchCase': 1}
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'warn',
      'always'
    ],
    'no-unused-vars': [
      'warn', { 'args': 'none' }
    ],
    'import/no-unresolved': ['error', {commonjs: true}],
    'no-undef': 'error',
    'lodash-template/plugin-option': [2, {
      'ignoreRules': ['no-undef'],
    }]
  }
};
