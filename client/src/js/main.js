(function ($) {
  $.analytics = function (options) {

    var defaults = {
      url: '',
      debug: false
    };

    var plugin = this;
    plugin.settings = {};

    plugin.init = function () {
      plugin.settings = $.extend({}, defaults, options);
      
      console.log('$.analytics settings:', plugin.settings);
      
      if (plugin.settings.user) {
        
        $('body').on('keypress click', function (e) {
          window.setTimeout(function () {
            var data = {
              user: plugin.settings.user
            };
            var result = '';
              
            if (e.type === 'click') {
              data.type = 'click';
              data.position = {
                x: e.pageX,
                y: e.pageY,
              };
            } else if(e.type === 'keypress') {
              data.type = 'keypress';
              data.keyCode = e.which;
            }
              
            result = JSON.stringify(data);
            plugin.settings.debug && console.log('analtics:', result);
              
            $.ajax({
              type: 'POST',
              url: plugin.settings.url,
              dataType: 'json',
              contentType: 'application/json',
              data: result,
              success: function (data, textStatus, jqXHR) {
                plugin.settings.debug && console.log('analitcs success', {
                  data: data,
                  jqXHR: jqXHR, 
                  textStatus: textStatus, 
                });
              },
              error: function (jqXHR, textStatus, errorThrown ) {
                textStatus &&
                console.log('analitics err', {
                  jqXHR: jqXHR, 
                  textStatus: textStatus, 
                  errorThrown: errorThrown
                });
              },
              complete: function () {
                plugin.settings.cb(result);
              }
            });
          },10);
        });
      }
    };

    plugin.init();
  };
}(jQuery));

(function ($) {
  function init() {
    $.analytics({
      url: '/api/analytics',
      user: $('body').attr('data-user'),
      cb: function (msg) {
        console.log('analytics request sent', msg);
      }
    });
  }
  
  init();
  
})(jQuery);
