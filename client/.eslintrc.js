module.exports = {
  'env': {
    'node': false,
    'browser': true,
    'es6': false
  },
  'extends': '../.eslintrc.js',
  'parserOptions': {
    'ecmaVersion': 5,
    'sourceType': 'script'
  }
};
